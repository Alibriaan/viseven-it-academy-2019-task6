import wizText from 'wiz-text';
import wizLayout from 'wiz-layout';
import wizImage from 'wiz-image';
import wizDivider from 'wiz-divider';
import wizButton from 'wiz-button';
import wizTitle from 'wiz-title';
import wizAgenda from 'wiz-agenda';
import wizLink from 'wiz-link';
import wizCard from 'wiz-card';
import wizCalendar from 'wiz-calendar';
import wizVideo from 'wiz-video';
import wizSocialFollowIcons from 'wiz-social-follow-icons';
import wizWrapper from 'wiz-wrapper';
import wizBlock from 'wiz-block';
import wizPlaceholderComponents from 'wiz-placeholder';

import PaddingTransfer from '../modules/padding-transfer';

export default function (Vue) {
  Vue.use(PaddingTransfer);
  Vue.use(wizPlaceholderComponents);
  Vue.component('wiz-wrapper', wizWrapper);
  Vue.component('wiz-block', wizBlock);
  Vue.component('wiz-layout', wizLayout);
  Vue.component('wiz-text', wizText);
  Vue.component('wiz-image', wizImage);
  Vue.component('wiz-divider', wizDivider);
  Vue.component('wiz-title', wizTitle);
  Vue.component('wiz-button', wizButton);
  Vue.component('wiz-agenda', wizAgenda);
  Vue.component('wiz-link', wizLink);
  Vue.component('wiz-card', wizCard);
  Vue.component('wiz-calendar', wizCalendar);
  Vue.component('wiz-social-follow-icons', wizSocialFollowIcons);
  Vue.component('wiz-video', wizVideo);
}
