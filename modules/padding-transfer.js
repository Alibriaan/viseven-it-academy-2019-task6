/* eslint-disable */
module.exports = {
  install(Vue) {
    Vue.mixin({
      methods: {
        $getPadding() {
          const td = this.$el.querySelector('td');
          const style = td && getComputedStyle(td);

          return {
            paddingTop: style.paddingTop,
            paddingRight: style.paddingRight,
            paddingBottom: style.paddingBottom,
            paddingLeft: style.paddingLeft,
            padding: style.padding,
          };
        },
        $isTableLayout() {
          const el = (this.$el.children[0] && this.$el.children[0].tagName === 'TBODY') ? this.$el.children[0] : this.$el;
          const child = el.childElementCount === 1 && el.children[0];
          const hasTableRow = child && child.tagName === 'TR';
          const grandChild = hasTableRow && child.childElementCount === 1 && child.children[0];
          const hasOnlyTd = grandChild && grandChild.tagName === 'TD';

          return hasOnlyTd;
        },
        $transferPadding() {
          const elComputedStyle = getComputedStyle(this.$el);
          const tdComputedStyle = getComputedStyle(this.$el.querySelector('td'));
          const tdStyle = this.$el.querySelector('td').style;

          const paddingTop = (parseInt(tdComputedStyle.paddingTop) && tdComputedStyle.paddingTop) || elComputedStyle.paddingTop;
          const paddingRight = (parseInt(tdComputedStyle.paddingRight) && tdComputedStyle.paddingRight) || elComputedStyle.paddingRight;
          const paddingBottom = (parseInt(tdComputedStyle.paddingBottom) && tdComputedStyle.paddingBottom) || elComputedStyle.paddingBottom;
          const paddingLeft = (parseInt(tdComputedStyle.paddingLeft) && tdComputedStyle.paddingLeft) || elComputedStyle.paddingLeft;

          tdStyle.paddingTop = '0';
          tdStyle.paddingRight = '0';
          tdStyle.paddingBottom = '0';
          tdStyle.paddingLeft = '0';
          tdStyle.padding = '0';

          this.$el.style.paddingTop = '';
          this.$el.style.paddingRight = '';
          this.$el.style.paddingBottom = '';
          this.$el.style.paddingLeft = '';

          this.$el.style.padding = `${paddingTop} ${paddingRight} ${paddingBottom} ${paddingLeft}`;
        },
      },
      mounted() {
        if (this.$isTableLayout()) {
          this.$transferPadding();
        }
      },
    });
  },
};
